import { UserService } from '../src/services/user.service'

let service: UserService

beforeAll(() => {
    service = new UserService()
})

describe('User Service', () => {
    describe('get', () => {
        it('should return a user with whatever ID you give it', () => {
            const id = Math.floor(Math.random() * 1000)

            const user = service.get(id)

            expect(user.id).toBe(id)
        })
        it('should return a user with whatever name you give it', () => {
            const id = Math.floor(Math.random() * 1000)
            const name = `name-${Math.random()}`

            const user = service.get(id, name)

            expect(user.id).toBe(id)
            expect(user.name).toBe(name)
        })
    })

    describe('create', () => {
        it('should return a user with the same email, name, and phone numbers that you provided', () => {
            const email = `email-${Math.random()}`
            const name = `name-${Math.random()}`
            const phoneNumber = `phone-number-${Math.random()}`

            const user = service.create({
                email,
                name,
                phoneNumbers: [phoneNumber],
            })

            expect(user.email).toBe(email)
            expect(user.name).toBe(name)
            expect(user.phoneNumbers[0]).toBe(phoneNumber)
        })
    })
})
