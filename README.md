# NodeJS, ExpressJS, Typescript REST API Starter

This repository is meant to be a base starter for a REST API using Node.js, Express, TypeScript, and an OpenAPI framework called TSOA.

Out of the box, this repository comes with:

-   TSOA implementation, which allows for automatic OpenAPI doc generation based on the code (viewable at /docs)
-   Logging, with exports to a logs/ directory
-   Automated testing using Jest
-   Automated linting with Prettier and ESLint
-   Pre-commit and pre-push hooks using Husky
    -   Pre-commit: Automatically lint the code
    -   Pre-push: Automatically run all unit tests
-   A build/test pipeline in Bitbucket
-   A couple pre-built routes and model examples to demonstrate how creating simple routes/docs works for TSOA

## Built With

This API server is built using ExpressJS with TypeScript and an Open API framework called TSOA to support automated API documentation. Please find a full list of tools used below.

-   [Node.js - Platform](https://nodejs.org/en/)
-   [TypeScript - Language](https://www.typescriptlang.org/)
-   [ExpressJS - Framework](https://expressjs.com/)
-   [TSOA - Framework](https://tsoa-community.github.io/docs/)
-   [Jest - Testing Suite](https://jestjs.io/)
-   [ESLint - Linting](https://eslint.org/)
-   [Prettier - Formatting](https://prettier.io/docs/en/)
-   [Husky - Git Hooks](https://typicode.github.io/husky/#/)
-   [Winston - Logging](https://github.com/winstonjs/winston)
-   [Morgan - Logging](https://github.com/expressjs/morgan)
-   [Yarn - Package Manager](https://yarnpkg.com/) (NPM will work fine though)

## Use

### Running Locally

1. Clone this repository
1. Run the command `yarn && yarn dev` (`npm i && npm run dev`)
1. Behold, this API server! Visit localhost:3000/docs for up-to-date Swagger documentation

### Linting

1. Run the command `yarn lint` (`npm run lint`) to get a list of everything that is formatted incorrectly
1. Run the command `yarn lint:fix` (`npm run lint:fix`) to make ESLint fix everything that it can fix automatically

### Testing

At this time, there are only unit tests for this repository.

1. Run the command `yarn test:unit` (`npm run test:unit`) to run all unit tests

## Author

If you have any questions about this repository or just want to chat, feel free to send an email or a chat to [Alex Hoopes](mailto:alexh@hstk.com)! I always love to chat. :)
