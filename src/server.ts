import { app } from './app'
import { name, version } from '../package.json'
import Logger from './util/logger'

const port = process.env.PORT || 3000

try {
    app.listen(port, () => {
        Logger.debug(`${name} (${version}) listening on port ${port}!`)
    })
} catch (error) {
    let message = 'Something went wrong!'
    if (error instanceof Error) {
        message = `Error occurred: ${error.message}`
    }

    Logger.error(message)
}
