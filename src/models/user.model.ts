/**
 * User objects allow you to associate actions performed
 * in the system with the user that performed them.
 * The User object contains common information across
 * every user in the system regardless of status and role.
 *
 * @example {
 *  "id": 23,
 *  "email": "jane@doe.com",
 *  "name": "Jane Doe",
 *  "status": "Happy",
 *  "phoneNumbers": ["111-111-1111"]
 * }
 */
export interface User {
    /**
     * The user's unique identifier
     * @example 23
     */
    id: number

    /**
     * The email the user registered their account with
     * @example "jane@doe.com"
     */
    email: string

    /**
     * The user's name
     * @example "Jane Doe"
     */
    name: string

    /**
     * The user's current status
     * @example "Happy"
     */
    status: 'Happy' | 'Sad'

    /**
     * All of the user's currently registered phone numbers.
     * @example ["111-111-1111"]
     */
    phoneNumbers: string[]
}
