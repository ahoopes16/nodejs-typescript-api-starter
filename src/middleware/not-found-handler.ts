import { Request as ExRequest, Response as ExResponse } from 'express'

export default function (_req: ExRequest, res: ExResponse): ExResponse {
    return res.status(404).send({
        message: 'Not Found',
    })
}
