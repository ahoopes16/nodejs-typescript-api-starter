import morgan, { StreamOptions } from 'morgan'

import isDevelopment from '../util/is-development'
import Logger from '../util/logger'

const stream: StreamOptions = {
    write: (message) => Logger.http(message),
}

const morganMiddleware = morgan(
    ':method :url :status :res[content-length] - :response-time ms',
    { stream, skip: () => !isDevelopment() }
)

export default morganMiddleware
