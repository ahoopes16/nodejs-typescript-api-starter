import {
    Request as ExRequest,
    Response as ExResponse,
    NextFunction,
} from 'express'
import { ValidateError } from 'tsoa'
import Logger from '../util/logger'

export default function (
    err: unknown,
    req: ExRequest,
    res: ExResponse,
    next: NextFunction
): ExResponse | void {
    if (err instanceof ValidateError) {
        Logger.warn(`Caught Validation Error for ${req.path}: `, err.fields)
        return res.status(422).json({
            message: 'Validation Failed',
            details: err?.fields,
        })
    }

    if (err instanceof Error) {
        return res.status(500).json({
            message: 'Internal Server Error',
        })
    }

    next()
}
