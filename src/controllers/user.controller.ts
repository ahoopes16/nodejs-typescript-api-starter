import {
    Body,
    Controller,
    Get,
    Path,
    Post,
    Query,
    Response,
    Route,
    SuccessResponse,
} from 'tsoa'
import { User } from '../models/user.model'
import { UserService, UserCreationParams } from '../services/user.service'
import { ValidateErrorJSON } from '../util/tsoa-responses'

@Route('users')
export class UserController extends Controller {
    /**
     * Retrieves the details of an existing user.
     * Supply the unique user ID and receive corresponding user details.
     * @param userId **Required.** The user's identifier
     * @param name **Optional.** The user's name
     * @summary Retrieve a user's information
     */
    @Response<ValidateErrorJSON>(422, 'Validation Failed')
    @Get('{userId}')
    public async getUser(
        @Path() userId: number,
        @Query() name?: string
    ): Promise<User> {
        return new UserService().get(userId, name)
    }

    /**
     * Creates a new user based on the information provided in the body.
     * Supply the user's email, name, and phone numbers and receive corresponding user details.
     * @param requestBody
     * @summary Create a new user
     */
    @Response<ValidateErrorJSON>(422, 'Validation Failed')
    @SuccessResponse('201', 'Created')
    @Post()
    public async createUser(
        @Body() requestBody: UserCreationParams
    ): Promise<User> {
        this.setStatus(201)
        return new UserService().create(requestBody)
    }
}
