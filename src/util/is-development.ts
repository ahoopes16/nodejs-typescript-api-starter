export enum ENVIRONMENTS {
    DEV = 'development',
    PROD = 'production',
}

export default (): boolean => {
    const env = process.env.NODE_ENV || ENVIRONMENTS.DEV
    return env === ENVIRONMENTS.DEV
}
