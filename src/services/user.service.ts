import { User } from '../models/user.model'

export type UserCreationParams = Pick<User, 'email' | 'name' | 'phoneNumbers'>

export class UserService {
    public get(id: number, name?: string): User {
        return {
            id,
            email: 'jane@doe.com',
            name: name || 'Jane Doe',
            status: 'Happy',
            phoneNumbers: [],
        }
    }

    public create(body: UserCreationParams): User {
        return {
            id: Math.floor(Math.random() * 1000),
            status: 'Happy',
            ...body,
        }
    }
}
