import express, {
    Application,
    Request as ExRequest,
    Response as ExResponse,
} from 'express'
import swaggerUI from 'swagger-ui-express'

import { RegisterRoutes } from '../dist/routes'
import notFoundHandler from './middleware/not-found-handler'
import httpLogger from './middleware/http-logger'
import errorHandler from './middleware/error-handler'

export const app: Application = express()

// Logging
app.use(httpLogger)

// Body Parsing
app.use(express.json())
app.use(
    express.urlencoded({
        extended: true,
    })
)

// Setup Swagger Docs
// TODO: Do not do this in production!
app.use('/docs', swaggerUI.serve, async (_req: ExRequest, res: ExResponse) => {
    return res.send(
        swaggerUI.generateHTML(await import('../dist/swagger.json'))
    )
})

// Register TSOA Routes
RegisterRoutes(app)

// Error Handling
app.use(notFoundHandler)
app.use(errorHandler)
